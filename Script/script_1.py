from shutil import copy
from shutil import copyfileobj
import csv
import os
import gzip

#Where is the list of machines in scope?
Scope  = open("C:/Users/bassmits/Documents/BI&A/Start package/Logbook downloader/Input Load Logbooks.csv","r")
ScopeList = list(csv.reader(Scope))
ScopeListClean = [] 

#Clean list
for Machines in ScopeList:
    ScopeListClean.append(Machines[0])

#Loop through machines
for Machines in ScopeListClean:
    print(Machines)
    
    #Startdate
    DateCompare = 20100101
    LogbookFound = ""
    
    #Set Machine directory
    file_dir = "X:/ALL/" + Machines + "/"
    
    if os.path.exists(file_dir):
    
        os.chdir(file_dir)
        all_txt_files = os.listdir(file_dir)

        for text in all_txt_files:
            if text.find("LOGBOOK") > 0:
                DateFound = int(text[:8])
                if DateFound > DateCompare:
                    LogbookFound = text
                    DateCompare = DateFound

        if LogbookFound != "":


            newpath = r"C:/Users/bassmits/Documents/BI&A/Start package/Logbook downloader/NewLogbooks3/" + Machines +"/"
            if not os.path.exists(newpath):
                os.makedirs(newpath)


            copy(file_dir + LogbookFound, "C:/Users/bassmits/Documents/BI&A/Start package/Logbook downloader/NewLogbooks3/" + Machines + "/")

            #with gzip.open("C:/Users/niweijde/Documents/DUV/Top 8/Logbooks/TestFolder2/" + LogbookFound, 'rb') as in_file:
            #    s = in_file.read()

            # store uncompressed file data from 's' variable
            #with open("C:/Users/niweijde/Documents/DUV/Top 8/Logbooks/TestFolder2/"+ Machines + "_ASML_Logbook.txt", 'w') as f:
            #    f.write(s)

            with gzip.open("C:/Users/bassmits/Documents/BI&A/Start package/Logbook downloader/NewLogbooks3/" + Machines + "/" + LogbookFound, 'rb') as f_in:
                with open("C:/Users/bassmits/Documents/BI&A/Start package/Logbook downloader/NewLogbooks3/" + Machines + "_ASML_Logbook.txt", 'wb') as f_out:
                    copyfileobj(f_in, f_out)


            #tar = gzip.open("C:/Users/niweijde/Documents/DUV/Top 8/Logbooks/TestFolder2/" + LogbookFound)
            #tar.extractall()
            #tar.close()
            #os.rename("C:/Users/niweijde/Documents/DUV/Top 8/Logbooks/TestFolder2/ASML_Logbook.txt","C:/Users/niweijde/Documents/DUV/Top 8/Logbooks/TestFolder2/"+ Machines + "_ASML_Logbook.txt" )

    else:
        print(Machines + " : No logbook found")
    
