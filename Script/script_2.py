# -*- coding: utf-8 -*-
"""
Created on Wed May 19 20:21:45 2021

@author: bassmits
"""

import re
import pandas as pd
import numpy as np
from dateutil import parser
import datetime
from itertools import islice
import dateutil.parser
import os 

def grab_machine_nr(file_dir, file_name):
    name = file_name.lower()
    name = name.replace("_asml_logbook.txt","")
    #name = str(file_name[file_name.find("_ASML_Logbook")-13:file_name.find("_ASML_Logbook")-len("_ASML_Logbook")-17])
    return name

def parse_logbook(file_dir, file_name):

    machine = grab_machine_nr(file_dir, file_name)
    print(machine)
    print(datetime.datetime.now().time())

    #Extract 4022. 
    material = []
    linenumber = []
    equipment = []
    pattern = r"40[.]?22\s?[:.]?\s?\d{2,3}[.]?\d{4,6}[*X]?|[Ss][Ee][Rr][Vv]\s?[:.]?\s?\d{3}[.]?\d{4,6}[*X]?|9428\s?[:.]?\s?\d{3}[.]?\d{4,6}|\s\d{3}[.]\d{5}\s"
    with open(file_name,'rt',errors='ignore') as in_file:
        for linenum,line in enumerate(in_file):
            sentence = line.upper()
            match = re.findall(pattern,sentence)
            if len(match) > 0:
                #print(match, linenum)
                material.extend(match)
                linenumber.extend([linenum]*(len(match)))
                equipment.extend([machine]*(len(match)))

    material = [i.replace(" ", "").rstrip().replace(".","").replace(":","") for i in material]
    material1 = ['{}.{}.{}'.format(i[:4],i[4:7],i[7:]) if len(i) == 12 else '{}.{}.{}1'.format(i[:4],i[4:7],i[7:]) if len(i) == 11 else '{}.{}.{}'.format(i[:4],i[4:7],i[7:12]) if len(i) == 13 else '4022.{}.{}'.format(i[:3],i[3:]) if len(i) == 8 else i for i in material]

    #Extract wt specific information
    wt = []
    linenumber6 = []
    equipment6 = []
    pattern = r"NEW\s?WT[^A-Z]|NEW\s?WAFER[^A-Z]|NEW\s?WAFER\s?TABLE[^A-Z]|OLD\s?WT[^A-Z]|OLD\s?WAFER[^A-Z]|OLD\s?WAFER\s?TABLE[^A-Z]|WT\sOLD|WT\sNEW|[Yy][Ee]?[Ee][Dd][Ee][Ee]?[Xx]|[Cc][Ee][Rr][Aa][Tt][Ee][Cc]|\s?[Rr][Ee][Pp][Ll][Aa][Cc][Ee]\s?[Ww][Tt]\s?|[Yy]351\-[Mm]008|[Yy]\d{3}\-[Mm]d{3}|\d{5}[EeAa]^.*[^-_]$|\W[Ii][Nn][Ee][Xx]"
    with open(file_name,'rt',errors='ignore') as in_file:
        for linenum,line in enumerate(in_file):
            sentence = line.upper().rstrip()
            match = re.findall(pattern,sentence)
            if len(match) > 0:
                #print(match)
                wt.extend(match)
                linenumber6.extend([linenum]*(len(match)))
                equipment6.extend([machine]*(len(match)))

    wt = [i.lstrip().replace(".","").replace(",","").replace("\t","").replace("_","") for i in wt]


    #Extract Dates
    dates = []
    linenumber2 = []
    equipment2 = []
    pattern = r"201[0-9][\-/]?\d\d[\-/]?\d\d|\d\d[\-/]?\d\d[\-/]?201[0-9]|\d{2}[\-//s]?[A-Z]{3}[\-//s]?2?0?1[0-9]{1}|2?0?1[0-9]{1}[\-//s]?[A-Z]{3}[\-//s]?\d{2}|\d{1,2}[\-//s]\d{1,2}[\-//s]\d{4}|[\s:]+\d{1,2}[\-//s]\d{1,2}[\-//s]\d{2}\s"
    with open(file_name,'rt',errors='ignore') as in_file:
        for linenum, line in enumerate(in_file):
            sentence = line.upper().rstrip()
            match = re.findall(pattern,sentence)
            if len(match) > 0:
                #print(linenum, match)
                try:
                    d = parser.parse(match[0])
                    day = d.strftime("%Y-%m-%d")
                    m = d.strftime("%Y-%m")
                    #print(m)
                except ValueError:
                    continue
                dates.append(day)
                linenumber2.append(linenum)
                equipment2.append(machine)


    #Extract new and old
    new_or_old = []
    linenumber3 = []
    equipment3 = []
    pattern = r"[(]?OLD[:)]?\s?|[(]?NEW[):]?\s?|DOA[:]?\s?|DEFECT|INSTALL|REMOV|\sIN[\s:=\n.]+|\sOUT"
    with open(file_name,'rt',errors='ignore') as in_file:
        for linenum, line in enumerate(in_file):
            sentence = line.upper().rstrip().replace("\t","")
            match = re.findall(pattern,sentence)
            if len(match) > 0:
                #print(match,linenum) 
                new_or_old.extend(match)
                linenumber3.extend([linenum]*(len(match)))
                equipment3.extend([machine]*(len(match)))

    #print(len(new_or_old))

    #Extract serial numbers
    serial_num = []
    linenumber4 = []
    equipment4 = []
    pattern = ("[RL]EX[=:-]?\s{0,2}[#]?\d{2,4}|[=:#_\s\(]{1,2}\s?[A-Z]?\d{4,6}\)?\s+\n?|S[/]?N[:#=]?\s{0,2}\d{3,5}\s?[),]?\s|[Jj][Oo]?\d{1,2}\.\d{4}")
    with open(file_name,'rt',errors='ignore') as in_file:
        for linenum, line in enumerate(in_file):
            sentence = line.upper()
            match = re.findall(pattern,sentence)
            if len(match) > 0:
                match = [i for i in match if '4022' not in i]
                #print(match,linenum)
                #print(len(match))
                #match = match[0].rstrip()
                #match = match.replace('\t','')
                serial_num.extend(match)
                linenumber4.extend([linenum]*(len(match)))
                equipment4.extend([machine]*(len(match)))

    serial_num = [i.rstrip().replace('\t','').replace(":","").replace("_","").replace("=","").replace(" ","").replace("#","").replace(")","").replace(",","") for i in(serial_num)]
    serial_num = [i.replace('4022',"").replace(" ","") if '4022' in i else i.replace('1980',"").replace(" ","") if i == "1980" else i for i in serial_num]
    #print(len(serial_num))

    #Extract '9 digits'
    bid = []
    linenumber5 = []
    equipment5 = []
    pattern = r"[\s:=#]?[S]?[N]?[1]\d{7,9}\s?[_]?"
    with open(file_name,'rt',errors='ignore') as in_file:
        for linenum, line in enumerate(in_file):
            sentence = line.upper().rstrip()
            match = re.findall(pattern,sentence)
            if len(match) > 0:
                #print(match,linenum)
                #print(len(match))
                #match = match[0].rstrip()
                #match = match.replace('\t','')
                bid.extend(match)
                linenumber5.extend([linenum]*(len(match)))
                equipment5.extend([machine]*(len(match)))

    bid = [i.replace(":","").replace("_","").replace("=","").replace("\t","").replace("#","").replace(" ","").replace("SN","") for i in bid]

    
    #extract chuck information
    chuck = []
    linenumber7 = []
    equipment7 = []
    pattern = r"[\s:][_]?[Cc][Hh][12]|[RL]EX|\s[_]?CK[12][\s.:]|[\s(:][Cc][Hh][Uu][Cc][Kk]\s?[12]|\sC[12][\s.:]"
    with open(file_name, 'rt',errors='ignore',encoding='utf-8') as in_file:
        for linenum, line in enumerate(in_file):
            if line == "":
                continue
            else:
                sentence = line.upper().rstrip()
                match = re.findall(pattern,sentence)
                if len(match) > 0:
                    #print(match,linenum)
                    #print(len(match))
                    #match = match[0].rstrip()
                    #match = match.replace('\t','')
                    chuck.extend(match)
                    linenumber7.extend([linenum]*(len(match)))
                    equipment7.extend([machine]*(len(match)))
                
    chuck = [i.lstrip().replace(".","").replace(")","").replace("#","").replace(":","").replace("\xa0","").replace(" ","").replace("=","").replace("(","") for i in chuck]
    chuck = [re.sub(r'\d+', '', x) if 'LEX' in x or 'REX' in x else x for x in chuck]

                
                
    #Make df's
    df = pd.DataFrame(list(zip(material1, linenumber, equipment)),
                columns=['clean 12NC','row','equipment'])

    df1 = pd.DataFrame(list(zip(wt, linenumber6, equipment6)),
                      columns=['clean 12NC','row','equipment']) 

    df = df.append(df1)


    df2 = pd.DataFrame(list(zip(dates, linenumber2, equipment2)),
                  columns=['date','row','equipment'])

    df3 = pd.DataFrame(list(zip(new_or_old, linenumber3, equipment3)),
                  columns=['new/old','row','equipment'])

    df4 = pd.DataFrame(list(zip(serial_num, linenumber4, equipment4)),
                  columns=['sn','row','equipment'])

    df5 = pd.DataFrame(list(zip(bid, linenumber5, equipment5)),
                  columns=['bid','row','equipment'])
    
    df7 = pd.DataFrame(list(zip(chuck, linenumber7, equipment7)),
                  columns=['chuck','row','equipment'])
    
    #closest algorithm
    chucks = []
    dates = []
    new_old = []
    serials = []
    bids = []
    sn_line = {}
    bid_line = {}
    counter = 0

    for i in range(len(df)):
        aux = []
        number = df.iloc[i]['row'] #Find the index of this number in other dataFrame
        slice_materials = df.loc[df['row'] == number]
        count_materials = slice_materials['clean 12NC'].tolist()
        count_materials = [count_materials[i] for i in range(len(count_materials)) if len((count_materials[i])) == 14]
        count_materials = len(count_materials)
        if count_materials == 1:
            counter = 1
        elif count_materials == 2 and counter == 0:
            counter += 3
        elif count_materials == 2 and counter == 2:
            counter += 0
        else:
            counter = 1
            
        try:
            d = df2.loc[df2['row'] <= number]
            d = d.iloc[(d['row']-number).abs().argsort()[:5]]
            try:
                #print(d)
                date = d['date'].tolist()
                #print(type(date[0]))
                dates.append(date[0])
            except IndexError:
                dates.append('no date found')
        except ValueError:
            dates.append('no date found')
        try:
            #new
            n = df3.loc[df3['row'] <= number]
            n = n.iloc[(n['row']-number).abs().argsort()[:2]]
            n = n.sort_index()
            n = n.iloc[(n['row']-number).abs().argsort()[:2]]
            try:
                if counter == 3:
                    new = n['new/old'].tolist()
                    new_old.append(new[0])
                    counter -= 1
                elif counter == 1:
                    new = n['new/old'].tolist()
                    new_old.append(new[0])
                    counter = 0
                else:
                    new = n['new/old'].tolist()
                    new_old.append(new[1])
                    counter = 0
            except IndexError:
                new_old.append('nothing nearby found')
        except ValueError:
            new_old.append('nothing nearby found')
        try:
            #sn
            s = df4.loc[df4['row'] >= number-2]
            s = s.iloc[(s['row']-number).abs().argsort()[:3]]
            #print(s)
            #NICK EDIT: Changed all <= 3 and >= -3 to 2
            try:
                if (number - s['row'].tolist()[0] <= 2) & (number - s['row'].tolist()[0] >= -2):
                    sn = s['sn'].tolist()
                    if sn[0] == "":
                        s = s.iloc[(s['row']-number).abs().argsort()[1:3]]
                        sn = s['sn'].tolist()
                        #print(sn)
                        if (number - s['row'].tolist()[0] <= 2) & (number - s['row'].tolist()[0] >= -2):
                            if (sn[0], s['row'].tolist()[0]) not in sn_line.items():
                                serials.append(sn[0])
                                sn_line[sn[0]] = s['row'].tolist()[0]
                            else:
                                if (number - s['row'].tolist()[1] <= 2) & (number - s['row'].tolist()[1] >= -2):
                                    if (sn[1], s['row'].tolist()[1]) not in sn_line.items():
                                        serials.append(sn[1])
                                        sn_line[sn[1]] = s['row'].tolist()[1]
                                    else:
                                        serials.append('no sn nearby found')
                                else:
                                    serials.append('no sn nearby found')
                        else:
                            serials.append('no sn nearby found')
                    else:
                        if (sn[0], s['row'].tolist()[0]) not in sn_line.items():
                            serials.append(sn[0])
                            sn_line[sn[0]] = s['row'].tolist()[0]
                        else:
                            if (number - s['row'].tolist()[1] <= 2) & (number - s['row'].tolist()[1] >= -2):
                                if (sn[1], s['row'].tolist()[1]) not in sn_line.items():
                                    serials.append(sn[1])
                                    sn_line[sn[1]] = s['row'].tolist()[1]
                                else:
                                    serials.append('no sn nearby found')
                            else:
                                serials.append('no sn nearby found')
                else:
                    serials.append('no sn nearby found')
            except IndexError:
                serials.append('no sn nearby found')
        except ValueError:
            serials.append('no sn nearby found')
        try:
            #bid
            b = df5.loc[df5['row'] >= number]
            b = b.iloc[(b['row']-number).abs().argsort()[:2]]
            #print(b)
            try:
                if (number - b['row'].tolist()[0] <= 0) & (number - b['row'].tolist()[0] >= 0):
                    bid = b['bid'].tolist()
                    if (bid[0], b['row'].tolist()[0]) not in bid_line.items():
                        bids.append(bid[0])
                        bid_line[bid[0]] = b['row'].tolist()[0]
                    else:
                        if (number - b['row'].tolist()[1] <= 0) & (number - b['row'].tolist()[1] >= 0):
                            bids.append(bid[1])
                            bid_line[bid[1]] = b['row'].tolist()[1]
                        else:
                            bids.append('no bid nearby found')
                else:
                    bids.append('no bid nearby found')
            except IndexError:
                bids.append('no bid nearby found')
        except ValueError:
            bids.append('no bid nearby found')
        try:
            c = df7.loc[df7['row'] <= number]
            c = c.iloc[(c['row']-number).abs().argsort()[:5]]
            #print(c)
            #print(number)
            try:
                if (number - c['row'].tolist()[0] <= 20) & (number - c['row'].tolist()[0] >= -5):
                    cuk = c['chuck'].tolist()
                    chucks.append(cuk[0])
                else:
                    chucks.append('no chuck found')
            except IndexError:
                chucks.append('no chuck found')
        except ValueError:
            chucks.append('no chuck found')

    #Append output to df
    df['date'] = dates
    df['new_old'] = new_old
    df['serial'] = serials
    df['bid'] = bids
    df['chuck'] = chucks
    
    for i in range(len(df)):
        for j in range(1,6):
            upperlimit = 0
            lowerlimit = 0
            if j == 1:
                upperlimit = -1
            elif j == 2:
                upperlimit = -2
            elif j ==3: 
                upperlimit = -3
            elif j ==4:
                lowerlimit = 1
            elif j ==5:
                lowerlimit = 2
            elif j ==6:
                lowerlimit = 3
                
                
            number = df.iloc[i]['row'] #Find the index of this number in other dataFrame
            bid_line = df['bid'].tolist()

            if df.iloc[i]['bid'] == 'no bid nearby found': #Only execute when no bid is found nearby      
                try:
                    #bid
                    b = df5.loc[df5['row'] >= number]
                    b = b.iloc[(b['row']-number).abs().argsort()[:2]]
                    try:
                        if (number - b['row'].tolist()[0] <= lowerlimit) & (number - b['row'].tolist()[0] >= upperlimit):
                            bid = b['bid'].tolist()
                            if (bid[0], b['row'].tolist()[0]) not in bid_line.items():
                                df.iloc[i]['bid'] = bid[0]
                            else:
                                if (number - b['row'].tolist()[1] <= lowerlimit) & (number - b['row'].tolist()[1] >= upperlimit) & (bid[1], b['row'].tolist()[1]) not in bid_line.items():
                                     df.iloc[i]['bid'] = bid[1]
                    except:
                        df.iloc[i]['bid'] = 'no bid nearby found'
                except:
                    df.iloc[i]['bid'] = 'no bid nearby found'
    
    # upper function in python to convert the character column to uppercase 
    df['upper_desc'] = list(map(lambda x: x.upper(), df['clean 12NC']))
    
    df = df.copy()
    df['new_old'] = df['new_old'].str.upper().str.strip().str.replace(':','').str.strip("()")
    df.head()
    
    return df

def allocate_date(df):
    
    df = df.reset_index(drop=True)
    LIMIT = df.shape[0]

    for index in islice(df.index, LIMIT):
        if index == 0:
            continue
        else:
            date = df['date'][index]
            previous_date = df['date'][index - 1]
            try:
                d = dateutil.parser.parse(previous_date) - dateutil.parser.parse(date)
                n = df['row'][index - 1] - df['row'][index] 
                if (d > datetime.timedelta(days=365)) and (n < 1500) and (n > -1500):
                    df.at[index, "date"] = previous_date
            except ValueError:
                continue
                
    return df

file_dir = 'C:/Users/bassmits/Documents/BI&A/Start package/Logbook downloader/NewLogbooks3'
os.chdir(file_dir)

all_txt_files = os.listdir(file_dir)
print(all_txt_files)

final_DF = pd.DataFrame() #creates a new dataframe that's empty

for text in all_txt_files:
    print(text)
    try: 
        df = parse_logbook(file_dir,text)
        #print(df)
        df = allocate_date(df)
        # store DataFrame in empty df
        df.to_excel('{} logbook output.xlsx'.format(text[:4]))
        final_DF = final_DF.append(df,ignore_index=True)
    except: 
        continue
        
final_DF.to_excel('all logbooks.xlsx')